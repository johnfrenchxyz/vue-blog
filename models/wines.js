// We need the mongoose object
const mongoose = require('mongoose');

// This is just an alias to save us typing time
const Schema = mongoose.Schema;

// Schema (mongoose.Schema) is a constructor function that takes in an object
// and returns a Schema object.
const wineSchema = new Schema({
	'name': String,	// String is a javascript literal object
	'cost': Number	// So is Number
});

// We then take that schema we made and pass it into the mongoose.model function
// the first argument is just a string that names the model, it can be whatever
// you want, but it's good to use the same terminology throughout.  The second
// argument is the schema object we got above.
const Wine = mongoose.model('Wine', wineSchema);

// Whatever module.exports is set to is what will be returned when you require
// this file
module.exports = Wine;
