const bluebird = require('bluebird');
const cors = require('cors');
const express = require('express');
const mongoose = require('mongoose');
const app = express();

const Wine = require('./models/wines.js')


// Database Test
mongoose.connect('mongodb://localhost/johnfrenchxyz');
mongoose.Promise = bluebird;

// Define the port
const port = 3000;

app.get('/', cors(), (req, res) => {
	Wine.find({}, (err, docs) => {
		if (err) console.error(err);
		res.send(docs);
	});
});

app.listen(port, () => console.log('App Listening on port ' + port));
