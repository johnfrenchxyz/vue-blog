import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Work from '@/components/Work'
import Writing from '@/components/Writing'
import Contact from '@/components/Contact'

import Login from '@/components/admin/Login'
import BlogAdmin from '@/components/admin/BlogAdmin'
import NewPost from '@/components/admin/NewPost'

const isLoggedIn = false;

Vue.use(Router)

// Explore Navigation guards to get the protected pages working!

export default new Router({
  routes: [
		{
			path: '/',
			name: 'Home',
			component: Home
		},
		{
			path: '/writing',
			name: 'Writing',
			component: Writing
		},
		{
			path: '/work',
			name: 'Work',
			component: Work
		},
		{
			path: '/contact',
			name: 'Contact',
			component: Contact
		},
    {
      path: '/admin',
      name: 'BlogAdmin',
			beforeEnter: (to, from, next) => {
				if (isLoggedIn === true) {
					next();
				} else {
					next({ path: '/login' });
				}
			},
      component: BlogAdmin
    },
		{
			path: '/new',
			name: 'NewPost',
			beforeEnter: (to, from, next) => {
				if (isLoggedIn === true) {
					next();
				} else {
					next({ path: '/login' });
				}
			},
			component: NewPost
		},
		{
			path: '/login',
			name: 'Login',
			component: Login
		}
  ]
})
